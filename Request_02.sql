SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Requête 2</TITLE> <link rel='stylesheet' href='style.css'>" BODY "><div><h1>Requête 2 :</h1> <p>Affichez le nom et le prénom des acheteurs qui ont payé des réservations ne contenant pas de billet à leur nom (donc pour quelqu’un d’autre). Triez les résultats par nom et prénom.</p></div"
spool request_02.html

SELECT DISTINCT(UPPER(c.last_name) || ' ' || INITCAP(c.first_name)) AS "Nom et Prenom"
FROM t_reservation r JOIN t_ticket t
ON r.buyer_id != t.customer_id
JOIN t_customer c
ON r.buyer_id = c.customer_id
ORDER BY "Nom et Prenom"
/

SET markup html off spool off
spool off