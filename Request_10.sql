SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Requête 10</TITLE> <link rel='stylesheet' href='style.css'>" BODY "><div><h1>Requête 10 :</h1> <p>OLVoyage souhaite faire une étude commerciale. Affichez le nombre de clients qui ont bénéficié de la réduction « Sénior » pour réserver des billets pour les trains partant au mois de janvier 2016. Ne prenez en compte que les réservations payées. Supposez que vous ne vous rappelez pas du nombre de jours dans le mois de janvier.</p></div"
spool request_10.html

SELECT COUNT(r.reservation_id) "Séniors de Janvier 2016"
FROM t_train t
JOIN t_wagon_train w
	ON t.train_id=w.train_id
JOIN t_ticket tk
	ON w.wag_tr_id=tk.wag_tr_id
JOIN t_reservation r
	ON r.reservation_id=tk.reservation_id
JOIN t_customer c 
	ON c.customer_id=r.buyer_id
JOIN t_pass p
	ON p.pass_id=c.pass_id
	WHERE p.pass_name='Senior'
		AND r.price IS NOT NULL
		AND TO_CHAR(t.post_time, 'MM/YY')='01/16'
/

SET markup html off spool off
spool off