SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Requête 15</TITLE> <link rel='stylesheet' href='style.css'>" BODY "><div><h1>Requête 15 :</h1> <p>Suite aux nombreux recrutements, OLVoyage souhaite établir un nouvel organigramme. Le nombre des réservations enregistrées par les employés doit suivre leur nom et leur prénom indentés. Etant donné que le dirigeant ne s’occupe pas des réservations, il ne doit pas apparaitre sur l’organigramme.</p></div"
spool request_15.html

SELECT
 UPPER(emp.last_name) ||' '|| INITCAP(emp.first_name) "Nom de l'Employé",
 count(t_reservation.reservation_id) "Réservations enregistrées"
FROM t_reservation
 RIGHT JOIN t_employee emp
 ON t_reservation.employee_id = emp.employee_id
WHERE manager_id is not null
GROUP BY emp.last_name, emp.first_name
ORDER BY count(t_reservation.reservation_id) DESC
/

SET markup html off spool off
spool off