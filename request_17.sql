SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Request_17</TITLE> <link rel='stylesheet' href='style.css'>" BODY "><div><h1>Requête 17 :</h1> <p>Les coordinateurs de ventes souhaitent disposer d’un outil simple et rapide pour enregistrer les paiements des réservations.<br/><br/>Créez un script interrogeant l’utilisateur, à chaque exécution, sur le numéro de la réservation et son type de paiement.<br/><br/>Le prix de la réservation doit être calculé et enregistré.<br/><br/>Un rapport sur la réservation doit également être créé. Le nom du fichier de rapport correspondra à l’exemple suivant : resa_NRéservation.htm (ex : resa_13.htm).<br/><br/>Enregistrez les paiements pour les réservations suivantes.<br/><br/><table><tr><th>Numéro de la réservation &nbsp</th><th>Type de paiement</th></tr><tr><td>13</td><td>CB</td></tr><tr><td>18</td><td>Especes</td></tr><tr><td>23</td><td>CB</td></tr><tr><td>31</td><td>Especes</td></tr></table><br/>Les types de paiement acceptés dans la colonne (sensibles à la casse)  sont :<ul><li>Cash</li><li>Cheque</li><li>Credit Card</li></p></div"
spool request_17.html

UPDATE t_reservation
SET buy_method=INITCAP('&moyen_de_paiement'), 
price=(SELECT SUM(CASE
       WHEN TO_CHAR(tr.post_time,'D') IN(6,7)
       THEN tr.price*(NVL(p.discount_we_pct,0)/100)
       WHEN TO_CHAR(tr.post_time, 'D') =5 AND TO_CHAR(tr.post_time,'hh24:mi')>'20:00'
       THEN tr.price*(NVL(p.discount_we_pct,0)/100)
       ELSE tr.price*(NVL(p.discount_pct,0)/100)
       END) "Prix de la résa"
       FROM t_reservation r
       JOIN t_ticket t
       ON r.reservation_id=t.reservation_id
       JOIN t_wagon_train w
       ON t.wag_tr_id=w.wag_tr_id
       JOIN t_train tr
       ON w.train_id=tr.train_id
       JOIN t_customer c
       ON t.customer_id=c.customer_id
       JOIN t_pass p
       ON c.pass_id=p.pass_id
       WHERE r.reservation_id='&numero_de_reservation')
WHERE reservation_id=&numero_de_reservation
/
 
SET markup html off spool off
spool off