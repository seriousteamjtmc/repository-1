SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Requête 9</TITLE> <link rel='stylesheet' href='style.css'>" BODY "><div><h1>Requête 9 :</h1> <p>Calculez la vitesse moyenne des trains. Utilisez l’affichage suivant : <b>90 km/h</b>. Pour chacun des trains, vous afficherez son numéro et son nom au format <b>Paris – Bordeaux</b>.</p></div"
spool request_09.html

SELECT t.train_id id, s.city||' - '||a.city "Trains", ROUND(distance/((arrival_time-post_time)*24))||' km/h' "Vitesse"
FROM t_train t 
JOIN t_station s
ON( t.departure_id = s.station_id)
JOIN t_station a
ON( t.arrival_id = a.station_id)
/

SET markup html off spool off
spool off