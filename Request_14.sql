SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Requête 14</TITLE> <link rel='stylesheet' href='style.css'>" BODY "><div><h1>Requête 14 :</h1> <p>Affichez le numéro du train, le nom du train et le nombre de places libres. Ne prendre en compte que les trains qui ont parcouru une distance supérieure à 300 Km le 22/01/2016 et pour lesquels des billets ont été réservés.<br/> Triez les résultats par le numéro du train.</p></div"
spool request_14.html
 
SELECT T.train_id "#Train", D.city ||' ==> ' || A.city "Destination", NbSieges.TotalMax - NbBilletsResa.AddResa "Places libres"
FROM (SELECT SUM(W.nb_site) TotalMax, T.train_id TrainAdd
FROM T_TRAIN T
JOIN T_WAGON_TRAIN TWAG
ON T.train_id = TWAG.train_id
JOIN T_WAGON W
ON ( TWAG.wagon_id = W.wagon_id )
GROUP BY T.train_id) NbSieges    
JOIN (SELECT COUNT(TKT.ticket_id) AddResa, T.train_id TrainComp
FROM T_TRAIN T
JOIN T_WAGON_TRAIN TWAG
ON T.train_id = TWAG.train_id
JOIN T_WAGON W
ON ( TWAG.wagon_id = W.wagon_id )
JOIN T_TICKET TKT
ON ( TWAG.wag_tr_id = TKT.wag_tr_id )
GROUP BY T.train_id) NbBilletsResa
ON ( NbSieges.TrainAdd = NbBilletsResa.TrainComp )    
JOIN T_TRAIN T
ON ( T.train_id = NbSieges.TrainAdd)
JOIN T_STATION D
ON ( D.station_id = T.departure_id )
JOIN T_STATION A
ON ( A.station_id = T.arrival_id )
WHERE T.distance > 300
AND T.arrival_time <= '02/01/16'
ORDER BY T.train_id

SET markup html off spool off
spool off