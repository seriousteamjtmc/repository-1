SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Requête 11</TITLE> <link rel='stylesheet' href='style.css'>" BODY "><div><h1>Requête 11 :</h1> <p>OLVoyage a besoin des statistiques sur les trains partant de Paris. Affichez les numéros des trains et les noms des trains avec le titre de chaque abonnement. Calculez également les tarifs comprenant chaque type de réduction pendant la semaine et le week-end. Ne pas prendre en compte les augmentations dues à la classe. Triez les résultats par numéro du train.</p></div"
spool request_11.html

SELECT DISTINCT  tr.train_id "Train", s.city || ' - ' ||
        st.city "Trains Départ PARIS", p.pass_name "Titre abonnement",
          (p.price-((p.discount_pct*p.price)/100)) "Tarif Semaine", (p.price-((discount_we_pct*p.price)/100)) "Tarif Week End"
FROM t_train tr
JOIN t_station s
ON tr.departure_id = s.station_id
AND s.city = 'Paris'
JOIN t_station st
ON (tr.arrival_id = st.station_id)
JOIN T_WAGON_TRAIN w
ON tr.train_id = w.train_id
JOIN T_TICKET t
ON w.wag_tr_id = t.wag_tr_id
JOIN T_CUSTOMER c
ON t.customer_id = c.customer_id
JOIN T_PASS p
ON c.pass_id = p.pass_id
ORDER BY tr.train_id
/

SET markup html off spool off
spool off