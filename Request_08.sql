SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Requête 8</TITLE> <link rel='stylesheet' href='style.css'>" BODY "><div><h1>Requête 8 :</h1> <p>Pour sa campagne publicitaire OLVoyage souhaite connaître le prénom, le nom et l’adresse des clients qui n’ont jamais eu d’abonnement. Triez les résultats par nom et prénom.</h1></div"
spool request_08.html

SELECT UPPER(first_name) || ' ' || INITCAP(last_name) "Nom et Prénom", address "Adresse" 
FROM T_CUSTOMER
WHERE pass_id IS NULL
ORDER BY last_name, first_name
/

SET markup html off spool off
spool off