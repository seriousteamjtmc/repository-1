SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Requête 16</TITLE> <link rel='stylesheet' href='style.css'>" BODY "><div><h1>Requête 16 :</h1> <p>Pour remercier ses coordinateurs de ventes, la société souhaite augmenter leur salaire de 100$. Affichez le numéro de l’employé, son nom et son prénom, ainsi que le salaire augmenté.<br/> Utilisez les sous-requêtes avancées.</p></div"
spool request_16.html


SELECT employee_id "N°",UPPER(last_name) || ' ' || first_name "Nom et Prénom", TO_CHAR(salary+100, '$99,999.00') "Salaire+100"
FROM t_employee
START WITH manager_id=(SELECT employee_id
					   FROM t_employee
					   WHERE manager_id IS NULL)
CONNECT BY PRIOR manager_id=employee_id
AND last_name != (SELECT last_name
				  FROM t_employee
				  WHERE manager_id IS NULL)
/

SET markup html off spool off
spool off