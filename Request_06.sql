SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Requête 6</TITLE> <link rel='stylesheet' href='style.css'>" BODY "><div><h1>Requête 6 :</h1> <p>La société OLVoyage souhaite faire des statistiques. Affichez les totaux pour les unités suivantes : employés, clients, pourcentage des clients abonnés (bien évidemment les calculs doivent exclure les abonnements périmés), trains, gares, réservations, billets. Prendre en compte les réservations et les billets non payés. <br/> Votre rapport ne contiendra qu’une seule ligne avec un total par colonne.</p></div"
spool request_06.html

SELECT
  Employee.NbEmp "Employés",
  Customer.NbCus "Clients",
  Customer.PrctAbon "Clients Abonnés (%)",
  Trains.NbTrain "Trains",
  Stations.NbSta "Gares",
  Reservation.NbRes "Réservations",
  Ticket.NbTic "Billets"
FROM (SELECT COUNT(DISTINCT e.employee_id) NbEmp
  FROM t_employee e) Employee,
  (SELECT COUNT(DISTINCT c.customer_id) NbCus,
  ROUND((COUNT(c.pass_id) / COUNT(DISTINCT c.customer_id)*100),2) || '%' PrctAbon
FROM t_customer c ) Customer,
  (SELECT COUNT( DISTINCT r.reservation_id) NbRes
FROM t_reservation r ) Reservation,
  (SELECT COUNT( DISTINCT t.ticket_id) NbTic
FROM t_ticket t ) Ticket,
  (SELECT COUNT( DISTINCT t.train_id) NbTrain
FROM t_train t ) Trains,
  (SELECT COUNT(DISTINCT s.station_id) NbSta
FROM t_station s) Stations
/

SET markup html off spool off
spool off