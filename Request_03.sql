SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Requête 3</TITLE> <link rel='stylesheet' href='style.css'>" BODY "><div><h1>Requête 3 :</h1> <p>Quelle a été la première réservation effectuée ? Affichez son numéro, sa date de création, le nom et le prénom de l’employé et de l’acheteur.</p></div"
spool request_03.html

/*set linesize 300 pour un meilleur affichage*/
SELECT r.reservation_id "La première réservation N°", TO_CHAR(r.creation_date, 'DD Month YYYY') "Date du",
UPPER(e.last_name) ||' '|| INITCAP(e.first_name) "Nom et Prénom de l'employé",
UPPER(c.last_name) ||' '|| INITCAP(c.first_name) "Nom et Prénom du conso."
FROM t_reservation r
	JOIN t_employee e USING(employee_id) /*Ici on utilise USING car męme nom de colonne ici on choisit employee_id*/
	JOIN t_customer c ON(c.customer_id=r.buyer_id) /*clause ON pour condition de jointure, pas de USING car nom de colonne <>*/
WHERE r.creation_date=(SELECT MIN(r.creation_date) /* SELECT dans un WHERE=sous requęte, voir Requętes avancées dans cours 4*/
FROM t_reservation r)
/

SET markup html off spool off
spool off