SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Requête 7</TITLE> <link rel='stylesheet' href='style.css'>" BODY "><div><h1>Requête 7 :</h1> <p>Affichez le Top-5 des trains (les 5 premiers trains) par rapport aux nombres de billets réservés. Le train doit être affiché de manière suivante <b>Paris – Brest</b>.</p></div"
spool request_07.html

SELECT *
FROM(SELECT d.city||'-'||a.city "Trajets Réalisés", COUNT(r.reservation_id) "Nombre de Reservations"
	FROM t_station d
		JOIN t_train t
	ON d.station_id = t.departure_id
		JOIN t_station a
	ON a.station_id = t.arrival_id
		JOIN t_wagon_train wt
	ON t.train_id = wt.train_id
		JOIN t_ticket tk
	ON tk.wag_tr_id = wt.wag_tr_id
		JOIN t_reservation r
	ON tk.reservation_id = r.reservation_id
		GROUP BY d.city||'-'||a.city
		ORDER BY "Nombre de Reservations" DESC)
WHERE ROWNUM <= 5
/

SET markup html off spool off
spool off