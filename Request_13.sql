SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Requête 13</TITLE> <link rel='stylesheet' href='style.css'>" BODY "><div><h1>Requête 13 :</h1> <p>Affichez le numéro du billet, le prénom et le nom du client ainsi que le train (même format que pour le rapport 5) pour les billets de première classe ayant été réservés par les clients de moins de 25 ans.<br/> Ne récupérez que les informations sur les réservations payées qui ont été faites 20 jours avant la date de départ du train.<br/> Ne prenez en compte que les trains qui partent entre le 15/04/2016 et le 25/04/2016.<br/> Triez les résultats par la date de la réservation.</p></div"
spool request_13.html

SELECT ti.ticket_id AS "Numero Billet", UPPER(cus.last_name) AS "Nom", INITCAP(cus.first_name) AS "Prénom",
 departure.city||'('||TO_DATE(tr.post_time,'DD/MM/YY HH24:MI')||')'||'-'||arrival.city||'('|| TO_DATE(tr.post_time, 'DD/MM/YY HH24:MI')||')' AS "Voyage"
FROM T_TRAIN tr
 JOIN T_STATION departure 
	ON tr.departure_id = departure.station_id
 JOIN T_STATION arrival 
	ON tr.arrival_id = arrival.station_id
 JOIN T_WAGON_TRAIN wagontr 
	ON tr.train_id = wagontr.train_id
 JOIN T_WAGON wagon 
	ON wagontr.wagon_id = wagon.wagon_id
 JOIN T_TICKET ti 
	ON wagontr.wag_tr_id = ti.wag_tr_id
 JOIN T_CUSTOMER cus 
	ON ti.customer_id = cus.customer_id
 JOIN T_RESERVATION res 
	ON ti.reservation_id = res.reservation_id
WHERE wagon.class_type LIKE '%1%'
   AND MONTHS_BETWEEN(SYSDATE, cus.birth_date) <= 12*25
   AND tr.post_time BETWEEN TO_DATE('15/01/16', 'DD/MM/YY') AND TO_DATE('25/01/16', 'DD/MM/YY')
   AND res.creation_date BETWEEN tr.post_time - 20 AND tr.post_time
   ORDER BY res.creation_date
 /
 
 SET markup html off spool off
spool off