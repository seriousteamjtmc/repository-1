SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Requête 5</TITLE> <link rel='stylesheet' href='style.css'>" BODY "><div><h1>Requête 5 :</h1> <p>Créez le rapport sur tous les trajets effectués par les trains OLVoyage. Affichez le numéro du train, le trajet qui est constitué de la ville de départ avec l’heure de départ et la ville d’arrivée avec l’heure d’arrivée, la distance parcourue et le prix initial.<br/> Exemple : <b>Nice (24/11/15 08:00) – Brest (24/11/15 16:10)</b><br/> Triez les résultats par numéro du train.</p></div"
spool request_05.html

SELECT t.train_id "Train N°",
        sdp.city || TO_CHAR(t.Post_time,'(DD/MM/YY HH24:MI)')||' - '|| sar.city ||
        TO_CHAR(t.arrival_time,'(DD/MM/YY HH24:MI)') "Trajet",
        t.distance "Distance en km", price ||'E' "Prix initial"
FROM t_train t
    JOIN t_station sd
        ON t.departure_id = sdp.station_id
    JOIN t_station sa
        ON t.arrival_id = sar.station_id
/

SET markup html off spool off
spool off