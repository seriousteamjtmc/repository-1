SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Requête 12</TITLE> <link rel='stylesheet' href='style.css'>" BODY "><div><h1>Requête 12 :</h1> <p>Affichez les titres des abonnements du plus vendu au moins vendu.</p></div"
spool request_12.html

SELECT pass_name "Nom du pass", COUNT(pass_id) "Nombre de Titres"
FROM T_CUSTOMER
NATURAL JOIN T_PASS
GROUP BY pass_name
ORDER BY COUNT(pass_id) DESC
/

SET markup html off spool off
spool off