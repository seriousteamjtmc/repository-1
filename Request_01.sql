SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Requête 1</TITLE> <link rel='stylesheet' href='style.css'>" BODY "><div><h1>Requête 1 :</h1> <p>OLVoyage souhaite connaître son meilleur employé. Affichez le prénom et le nom de l’employé qui a enregistré le plus grand nombre de réservations.</p></div"
spool request_01.html

SELECT *
FROM
	(SELECT UPPER(last_name) || ' ' || INITCAP(first_name) AS "Nom et Prénom", COUNT(reservation_id) "Nombre de réservations"
		FROM t_employee e
		JOIN t_reservation r
		ON r.employee_id = e.employee_id
		GROUP BY last_name, first_name
		ORDER BY COUNT(reservation_id)DESC)
WHERE ROWNUM<=1
/

SET markup html off spool off
spool off