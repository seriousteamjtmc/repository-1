# **A l'attention de Mr VITA Sebastien,** #

Je vous informe que notre projet a atteint sa phase finale. Pour y arriver j'ai commencé par affecter des requêtes à chacun des membres du groupe. 
### Partage du travail: 

**Jules:** requêtes 3,5,10,16,17.

**Thomas:** requêtes 8,11,12 et 14.

**Clément**: requêtes 2,4 et 9.

**Matteo:** requêtes 1,6,7,13 et 15.


Ainsi j'ai souhaité qu'il y ai tout d'abord un travail personnel sur les requêtes puis nous nous sommes réunis une fois par semaine pour constater l'avancement du projet et pour s'entraider suivant les difficultés rencontrées. 
Grâce à ces moments de regroupement, j'ai pu modifier certains objectifs suivant les membres, ainsi Clément s'est occupé de la mise en forme des requêtes en HTML et CSS. Malgré un nombre de requête moins important il a quand même pris pleinement part au développement du projet. 

### Difficultés rencontrées: 

Notre première difficulté a été l'utilisation de SourceTree et de Bitbucket qui n'a pas été évidente pour tout le monde. Nous avions un message d'erreur lors du Push. Après quelques recherches j'ai pu pallier au problème en rentrant ces deux lignes de code dans l'invite de commande SourceTree:

```
git config --global user.email "you@example.com" 
git config --global user.name "Your Name"


```

Chacun d'entre nous s'est heurté à des impasses pour arriver au bon fonctionnement des requêtes, mais nous avons pu y mettre un terme au fur et à mesure de notre avancement durant les cours en classe. Nous viendrons plus en détail sur ces dernières lors de notre soutenance orale.
**Brièvement:**

1. Les sous requêtes étaient notre première "découverte" ne les ayant pas encore travaillées en cours au moment de notre avancée.

2. Les jointures nous ont obligés à nous y reprendre à de multiples reprises et à nous obliger à être plus propres dans notre code et plus cohérents. 

3. Les fonctions de groupe nous ont aussi demandé de la rigueur et de la réflexion dans notre syntaxe. 

4. Les dates, en dehors du fait des modifications souhaitables pour obtenir des résultats, n'étaient pas toujours faciles à manipuler.

5. La mise en forme des colonnes et leur cohérence avec l'énoncé de la requête et des conditions exigées dans les annexes du créateur d'OLVoyage.   


### Résultat en HTML

Afin d'afficher correctement les requêtes générés en html, veuillez utiliser un navigateur récent comme google chrome ou mozilla firefox.


### Préparation de la soutenance orale: 

J'ai demandé aux membres de mon groupe de me faire parvenir les problématiques sur lesquelles ils souhaitent s'exprimer. Ainsi je vais élaborer une première version de notre support pour l'oral de vendredi prochain. Durant la semaine prochaine, nous allons nous entraîner sur ce support et le modifier si nécessaire pour une soutenance qui sera, je l'espère, d'une qualité optimale !






```
                                                                                                                                                                  SELECT cordialement
                                                                                                                                                                  FROM serious_team
                                                                                                                                                                  WHERE last_name IN (Aubel, Roy, LeFrançois, Jones)
```