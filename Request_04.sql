SET MARKUP HTML ON SPOOL ON PREFORMAT OFF ENTMAP ON HEAD "<TITLE>Requête 4</TITLE> <link rel='stylesheet' href='style.css'>" BODY "><div><h1>Requête 4 :</h1> <p>En prévision de la rentrée, OLVoyage prévoie une campagne publicitaire pour vendre ses abonnements. À la date du 30 septembre 2016, affichez les noms et les prénoms des clients avec leur titre d’abonnement. Si le client a un abonnement périmé, affichez le titre d’abonnement avec <b>Périmé !</b>, si le client n’a aucun abonnement affichez <b>Aucun</b>.<br/> Les titres d’abonnements sont valables un an à compter de la date de souscription.<br/> Triez les résultats par nom et prénom.</p></div"
spool request_04.html

SELECT UPPER(c.last_name) || ' ' || INITCAP(c.first_name) "Nom et Prénom", p.pass_name "Nom du pass",
CASE
 WHEN c.pass_date < TO_DATE('30/09/16', 'DD/MM/YY') THEN 'Périmé'
 WHEN c.pass_date IS NULL THEN 'Aucun'
 ELSE p.pass_name
 END pass
FROM T_CUSTOMER c JOIN T_PASS p
ON (c.pass_id = p.pass_id)
/

SET markup html off spool off
spool off